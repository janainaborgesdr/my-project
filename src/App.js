import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "./App.css";
import Home from "./components/Home";
import Navbar from "./components/Navbar";

const App = () => {
    return (
        <div className="App">
            <ToastContainer />
            <Router>
                <Navbar />
                <Router>
                    <Route path="/" component={Home} />
                    {/* <Route path="/add" component={} /> */}
                </Router>
            </Router>
        </div>
    );
};

export default App;
